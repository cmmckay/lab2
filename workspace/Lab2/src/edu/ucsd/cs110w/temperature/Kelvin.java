/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cmmckay
 *
 */
public class Kelvin extends Temperature {

	public Kelvin(float t) {
		super(t);
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toCelsius()
	 */
	public Temperature toCelsius() {
		float k = getValue();
		float c = k - 273;
		return new Celsius(c);
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toFahrenheit()
	 */
	public Temperature toFahrenheit() {
		float k = getValue();
		float f = ((k-273) * 9/5) + 32;
		return new Fahrenheit(f);
	}
	
	public Temperature toKelvin() {
		return this;
	}
	
	public String toString(){
		return getValue() + " K";
	}

}
