/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cmmckay
 *
 */
public class Celsius extends Temperature {

	public Celsius(float t) {
		super(t);
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toCelsius()
	 */
	public Temperature toCelsius() {
		return this;
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toFahrenheit()
	 */
	public Temperature toFahrenheit() {
		float c = getValue();
		float f = (c * 9/5) + 32;
		return new Fahrenheit(f);
	}
	
	public Temperature toKelvin() {
		float c = getValue();
		float k = c + 273;
		return new Kelvin(k);
	}
	
	public String toString(){
		return getValue() + " C";
	}

}
