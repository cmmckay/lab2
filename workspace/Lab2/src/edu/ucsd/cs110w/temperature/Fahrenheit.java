/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cmmckay
 *
 */
public class Fahrenheit extends Temperature {

	public Fahrenheit(float t) {
		super(t);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toCelsius()
	 */
	public Temperature toCelsius() {
		float f = getValue();
		float c = (f - 32) * 5/9;
		return new Celsius(c);
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toFahrenheit()
	 */
	public Temperature toFahrenheit() {
		return this;
	}

	public Temperature toKelvin() {
		float f = getValue();
		float k = (f - 32) * 5/9 + 273;
		return new Kelvin(k);
	}
	
	public String toString(){
		return getValue() + " F";
	}
}
