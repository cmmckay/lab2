package edu.ucsd.cs110w.tests;

import junit.framework.TestCase;
import edu.ucsd.cs110w.temperature.Celsius;
import edu.ucsd.cs110w.temperature.Fahrenheit;
import edu.ucsd.cs110w.temperature.Kelvin;
import edu.ucsd.cs110w.temperature.Temperature;

public class FarhenheitTests extends TestCase {
	private static final float delta = 0.001f;
	public void testFarenheit(){
		float value = 12.34f;
		Fahrenheit temp = new Fahrenheit(value);
		assertEquals(value, temp.getValue(), delta);
	}
	public void testCelsiusToString(){
		float value = 12.34f;
		Fahrenheit temp = new Fahrenheit(value);
		String string = temp.toString();String beginning = "" + value;
		String ending = " F";
		//Verify the suffix of the formatted string
		assertTrue(string.startsWith(beginning));
		//Verify the prefix of the formatted string
		assertTrue(string.endsWith(ending));
		//Verify the middle of the formatted string
		int endIndex = string.indexOf(ending);
		//Hint: what is the length of themiddle of the string?)
		assertTrue(string.substring(0, endIndex).equals(beginning));
	}
	public void testFahrenheitToFarenheit(){
		Fahrenheit temp = new Fahrenheit(0);
		Temperature convert = temp.toFahrenheit();
		assertEquals(0, convert.getValue(), delta);
	}
	public void testFahrenheitToCelsius(){
		Fahrenheit temp = new Fahrenheit(32);
		Temperature convert = temp.toCelsius();
		assertEquals(0, convert.getValue(), delta);
		temp = new Fahrenheit(212);
		convert = temp.toCelsius();
		
		assertEquals (100, convert.getValue(), delta);
	}
	
	public void testFahrenheitToKelvin(){
		Fahrenheit temp = new Fahrenheit(32);
		Temperature convert = temp.toKelvin();
		assertEquals(273, convert.getValue(), delta);
	}
}
